package u02lab.code;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;
import static u02lab.code.GeneratorTestHelper.consumeAllNumbers;

public class TestRandomGenerator {

    private static final int GENERATED_AMOUNT = 10;

    private SequenceGenerator generator;

    @Before
    public void setUp() {
        this.generator = new RandomGenerator(GENERATED_AMOUNT);
    }

    @After
    public void reset() {
        this.generator.reset();
    }

    @Test
    public void generatedNumbersAreBinary() {
        final List<Integer> elements = generator.allRemaining();
        assertTrue(elements.stream().allMatch(i -> i == 0 || i == 1));
    }

    @Test
    public void generatesSameAmount() {
        assertFalse(generator.isOver());
        assertEquals(generator.allRemaining().size(), GENERATED_AMOUNT);
        assertTrue(generator.isOver());

        generator.reset();

        assertFalse(generator.isOver());
        assertEquals(generator.allRemaining().size(), GENERATED_AMOUNT);
        assertTrue(generator.isOver());
    }

    @Test
    public void yieldsEmptyAfterStop() {
        consumeAllNumbers(generator);
        assertTrue(generator.isOver());
        assertFalse(generator.next().isPresent());
    }

}
