package u02lab.code;

class GeneratorTestHelper {
    static void consumeAllNumbers(final SequenceGenerator generator) {
        while (!generator.isOver()) {
            generator.next();
        }
    }

    static void consumeSomeNumbers(final SequenceGenerator generator, final int amount) {
        for (int i = 0; i < amount; i++) {
            generator.next();
        }
    }
}
