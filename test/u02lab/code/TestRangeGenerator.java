package u02lab.code;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.*;
import static u02lab.code.GeneratorTestHelper.consumeAllNumbers;
import static u02lab.code.GeneratorTestHelper.consumeSomeNumbers;

public class TestRangeGenerator {

    private static final int START = 1;
    private static final int STOP = 5;

    private SequenceGenerator generator;

    @Before
    public void setUp() {
        this.generator = new RangeGenerator(START, STOP);
    }

    @After
    public void reset() {
        this.generator.reset();
    }

    @Test
    public void rangeIsCovered() {
        for (int i = START; i <= STOP; i++) {
            final Optional<Integer> generatedNumber = generator.next();
            assertTrue(generatedNumber.isPresent());
            assertEquals(generatedNumber, Optional.of(i));
        }

        assertTrue(generator.isOver());
    }

    @Test
    public void yieldsEmptyAfterStop() {
        consumeAllNumbers(generator);
        assertTrue(generator.isOver());
        assertFalse(generator.next().isPresent());
    }

    @Test
    public void resetWorks() {
        final int half = (STOP - START) / 2;

        consumeSomeNumbers(generator, half);
        generator.reset();

        assertEquals(generator.next(), Optional.of(START));
    }

    @Test
    public void isOverIsConsistent() {
        final int half = (STOP - START) / 2;

        consumeSomeNumbers(generator, half);
        assertFalse(generator.isOver());

        consumeAllNumbers(generator);
        assertTrue(generator.isOver());
    }

    @Test
    public void allRemainingConsumesGenerator() {
        generator.next();
        generator.next();
        assertFalse(generator.isOver());

        assertEquals(generator.allRemaining(), Arrays.asList(3, 4, 5));
        assertTrue(generator.isOver());
    }
}
