package u02lab.code;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop leads to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and should give false, at the end it gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    private final int start;
    private final int stop;

    private Iterator<Integer> sequenceIterator;

    public RangeGenerator(int start, int stop){
        this.start = start;
        this.stop = stop;
        this.initializeIterator();
    }

    @Override
    public Optional<Integer> next() {
        return !this.isOver() ?
                Optional.of(this.sequenceIterator.next()) :
                Optional.empty();
    }

    @Override
    public void reset() {
        this.initializeIterator();
    }

    @Override
    public boolean isOver() {
        return !this.sequenceIterator.hasNext();
    }

    @Override
    public List<Integer> allRemaining() {
        final List<Integer> remaining = new ArrayList<>();
        this.sequenceIterator.forEachRemaining(remaining::add);
        return remaining;
    }

    private void initializeIterator() {
        this.sequenceIterator = IntStream.rangeClosed(this.start, this.stop).boxed().iterator();
    }
}
