package u02lab.code;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Step 2
 *
 * Now consider a RandomGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (RandomGenerator vs RangeGenerator), using patterns as needed
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private static final int LOWER_BOUND_INCLUSIVE = 0;
    private static final int UPPER_BOUND_EXCLUSIVE = 2;

    private final int sequenceLength;
    private Iterator<Integer> sequenceIterator;

    public RandomGenerator(int n){
        this.sequenceLength = n;
        this.initializeIterator();
    }

    @Override
    public Optional<Integer> next() {
        return !this.isOver() ?
                Optional.of(this.sequenceIterator.next()) :
                Optional.empty();
    }

    @Override
    public void reset() {
        this.initializeIterator();
    }

    @Override
    public boolean isOver() {
        return !this.sequenceIterator.hasNext();
    }

    @Override
    public List<Integer> allRemaining() {
        final List<Integer> remaining = new ArrayList<>();
        this.sequenceIterator.forEachRemaining(remaining::add);
        return remaining;
    }

    private void initializeIterator() {
        this.sequenceIterator = new Random().ints(LOWER_BOUND_INCLUSIVE, UPPER_BOUND_EXCLUSIVE)
                .boxed()
                .limit(this.sequenceLength)
                .iterator();
    }
}
